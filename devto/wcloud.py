import wikipedia
from wordcloud import WordCloud,STOPWORDS
import matplotlib.pyplot as plt 

page = wikipedia.page("Natural Language Processing")
text = page.content 


stopwords = set(STOPWORDS)
stopwords.update(["generally","using","usually","often","given","used"])

wcloud = WordCloud(stopwords=stopwords, background_color="white")
cloud = wcloud.generate(text)

plt.figure(figsize=(8,8), facecolor=None)
plt.imshow(cloud,interpolation='bilinear')
plt.axis("off")
plt.tight_layout(pad=0)
plt.show() 


