# Machine Learning And Data Science Expert

This Markdown will be used to retain key concepts to look up, probably, from each section of the course. 

## Machine Learning 101 

Machine Learning: make machine act more like human.

> we have a lot of topics listed such as: 
> computer vision
> natural language processing
> stock predictions
> etc 

make the machine act without being specificaly programmed 

AI / Machine Learning / DeepLearning / Data Science = work with data 


Data history for decision making:
  - spreadsheets (basic Data)
  - relational database (structured data with relational)
  - non relational database (big data is unstructured)

Machine Learning will use this data to make decisions for us. 

Machine Learning: supervised, unsupervised, transfert and reinforcement

## Framework

1. part of ML projects:
  - data collection
  - data modeling
  - deployment


[framework](https://www.mrdbourke.com/a-6-step-field-guide-for-building-machine-learning-projects/) 

1. problem definition
2. type of data 
3. evaluation metric
4. features of data 
5. modeling
6. experimentation

split data into 3 sets:
- training (70-80%)
- validation (15-10%)
- test (15-10%)

overfitting vs underfitting 


## Data Analysis Tools

[scipy](https://www.scipy.org/) ecosystem:
  1. Numpy: Base N-dimensional array package
  2. scipy: Fundamental library for scientific computing
  3. matplotlib: Comprehensive 2-D plotting
  4. ipython: Enhanced interactive console
  5. sympy: Symbolic mathematics
  6. pandas: Data structures and Analysis


- [pandas](https://pandas.pydata.org) will help with Handling data.
- [numpy](https://numpy.org/) will help us set efficient operation (numerical computing) on data.
-





- notebooks are the indicators
- pandas online documentation is great
- cheatsheet avalaible in ressources






